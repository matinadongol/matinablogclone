from rest_framework import serializers
from snippets.models import Snippet, LANGUAGE_CHOICES, STYLE_CHOICES


class SnippetSerializer(serializers.ModelSerializer):
    class Meta:
        model = Snippet
        fields = "__all__"

    # fields = ['id', 'title', 'code', 'linenos', 'language', 'style']

    #
    def create(self, validated_data):
        print(validated_data)
        return Snippet.objects.create(**validated_data)

    # def update(self, instance, validate_data):
    #
    #     instance.title = validate_data.get('title', instance.title)
    #     print("___________")
    #     print(instance.title)
    #     instance.code = validate_data.get('code', instance.code)
    #     instance.linenos = validate_data.get('linenos', instance.linenos)
    #     instance.language = validate_data.get('language', instance.language)
    #     instance.style = validate_data.get('style', instance.style)
    #     instance.save()
    #
    #     return instance


#  instance.update(
#             title=data["title"],
#             code=data["code"],
#             linenos=data["linenos"],
#             language=data["language"],
#             style=data["style"],
#         )
