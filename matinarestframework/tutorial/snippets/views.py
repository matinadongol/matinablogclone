from django.shortcuts import render, reverse, redirect
from snippets.serializers import SnippetSerializer
from snippets.models import Snippet
from snippets.models import *
from django.http import Http404, HttpResponse, HttpResponseRedirect
from django.http import JsonResponse
from django.views.generic import TemplateView

from django.views.decorators.csrf import csrf_exempt
from django.utils.decorators import method_decorator
from django.views.generic import View

from rest_framework.parsers import JSONParser
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework import viewsets, mixins, generics


# APIView
class SnippetAPIView(APIView):
    def get(self, request):
        snip = Snippet.objects.all()
        serializer = SnippetSerializer(snip, many=True)
        return Response(serializer.data, status=200)

    def post(self, request):
        data = request.data
        serializer = SnippetSerializer(data=data)
        print(serializer)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=201)
        return Response(serializer.errors, status=400)


class SnippetDetailView(APIView):
    def get_object(self, id):
        try:
            instance = Snippet.objects.get(id=id)
            return instance
        except Snippet.DoesNotExist as e:
            return Response({"error": "Given snippet object not found"}, status=404)

    def get(self, request, id=None):
        instance = Snippet.objects.get(id=id)
        serializer = SnippetSerializer(instance)
        return Response(serializer.data)


class SnippetUpdateView(APIView):
    def get_object(self, id):
        try:
            instance = Snippet.objects.get(id=id)
            return instance
        except Snippet.DoesNotExist as e:
            return Response({"error": "Given snippet object not found"}, status=404)

    def put(self, request, id=None):
        print("*********************************")
        data = request.data
        print(data)
        instance = self.get_object(id)
        print(instance)
        serializer = SnippetSerializer(instance, data=data)
        print(serializer)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=200)
        return Response(serializer.errors, status=400)


class SnippetDeleteView(APIView):
    def get_object(self, id):
        try:
            instance = Snippet.objects.get(id=id)
            return instance
        except Snippet.DoesNotExist as e:
            return Response({"error": "Given snippet object not found"}, status=404)

    def delete(self, request, id=None):
        instance = self.get_object(id)
        instance.delete()
        snip = Snippet.objects.all()
        serializer = SnippetSerializer(snip, many=True)
        return Response(serializer.data, status=204)


class Snippethtml(TemplateView):
    template_name = "snippets/snippets.html"


# function based
# @csrf_exempt
# def snippets(request):
#     if request.method == 'GET':
#         snip = Snippet.objects.all()
#         serializer = SnippetSerializer(snip, many = True)
#         return JsonResponse(serializer.data, safe=False)
#
#     elif request.method == 'POST':
#         json_parser = JSONParser()
#         data = json_parser.parse(request.POST)
#         serializer = SnippetSerializer(data=data)
#         if serializer.is_valid():
#             serializer.save()
#             return JsonResponse(serializer.data, status = 201)
#         return JsonResponse(serializer.errors, status = 400)


# @csrf_exempt
# def snippets_details(request, id):
#     try:
#         instance = Snippet.objects.get(id=id)
#     except Snippet.DoesNotExist as e:
#         return JsonResponse( {"error": "Given snippet object not found"}, status = 404)
#
#     if request.method == 'GET':
#         serializer = SnippetSerializer(instance)
#         return JsonResponse(serializer.data)
#
#     elif request.method == 'PUT':
#         json_parser = JSONParser()
#         data = json_parser.parse(request)
#         serializer = SnippetSerializer(instance, data=data)
#         if serializer.is_valid():
#             serializer.save()
#             return JsonResponse(serializer.data, status = 200)
#         return JsonResponse(serializer.errors, status = 400)
#
#     elif request.method == 'DELETE':
#         instance.delete()
#         return HttpResponse(status = 204)

# using mixins
# class SnippetList(generics.GenericViewSet,
#                   mixins.ListModelMixin,
#                   mixins.CreateModelMixin):
#     serializer_class = SnippetSerializer
#     queryset = Snippet.objects.all()
#
#     def get(self, request, *args, **kwargs):
#         return self.list(request, *args, **kwargs)
#
#     def post(self, request, *args, **kwargs):
#         return self.create(request, *args, **kwargs)
#
#
# class SnippetDetail(mixins.RetrieveModelMixin,
#                     mixins.UpdateModelMixin,
#                     mixins.DestroyModelMixin,
#                     generics.GenericAPIView):
#     serializer_class = SnippetSerializer
#     queryset = Snippet.objects.all()
#
#     def get(self, request, *args, **kwargs):
#         return self.retrieve(request, *args, **kwargs)
#
#     def put(self, request, *args, **kwargs):
#         return self.update(request, *args, **kwargs)
#
#     def delete(self, request, *args, **kwargs):
#         return self.destroy(request, *args, **kwargs)

# generic views
# class SnippetList(generics.ListCreateAPIView):
#     queryset = Snippet.objects.all()
#     serializer_class = =SnippetSerializer
#
#
# class SnippetDetail(generics.RetrieveUpdateDestroyAPIView):
#     queryset = Snippet.objects.all()
#     serializer_class = =SnippetSerializer

