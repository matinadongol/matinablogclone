from django.urls import path
from snippets.views import *

urlpatterns = [
    # path('snippets/', snippets),
    path("snippets/", SnippetAPIView.as_view()),
    path("snippets/<int:id>/", SnippetDetailView.as_view()),
    path("snippets/delete/<int:id>/", SnippetDeleteView.as_view()),
    path("snippets/update/<int:id>/", SnippetUpdateView.as_view()),
    # path('snippets/<int:id>/', snippets_details),
]
