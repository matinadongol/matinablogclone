function snippet(data){
    console.log(data);
    console.log(data.length);
    var html = '';
    html += '<table border = "2" class = "table table-hover">' +
        '<tr>'+
            '<th>Id</th>'+
            '<th>Created</th>'+
            '<th>Title</th>'+
            '<th>Code</th>'+
            '<th>Linenos</th>'+
            '<th>Language</th>'+
            '<th>Style</th>'+
        '</tr>'

        for (j=0; j<data.length; j++) {
            html += '<tr>' +
                '<td>'+ data[j]['id']+'</td>'+
                '<td>'+ data[j]['created']+'</td>'+
                '<td>'+ data[j]['title']+'</td>'+
                '<td>'+ data[j]['code']+'</td>'+
                '<td>'+ data[j]['linenos']+'</td>'+
                '<td>'+ data[j]['language']+'</td>'+
                '<td>'+ data[j]['style']+'</td>'+
            '</tr>'
        }
    html += '</table>';
    $('#snippet').append(html);
}


$.ajax({
    url: '/api/v1/snippets/',
    success: function (data) {
        snippet(data);
    }

});
