console.log('this is t')
alert('yo')
var snippets = [
    {
        "id": 1,
        "created": "2019-07-18T08:51:39.040595Z",
        "title": "",
        "code": "foo = \"bar\b",
        "linenos": false,
        "language": "python",
        "style": "friendly"
    },
    {
        "id": 2,
        "created": "2019-07-18T08:52:17.647101Z",
        "title": "",
        "code": "print(\"hello, world\")\n",
        "linenos": false,
        "language": "python",
        "style": "friendly"
    },
    {
        "id": 3,
        "created": "2019-07-18T09:11:36.129213Z",
        "title": "",
        "code": "fpp = \"bar\"\n",
        "linenos": false,
        "language": "python",
        "style": "friendly"
    },
 ];

 var html = '';
 html += '<table border = "2" class = "table table-hover">' +
    '<tr>'+
        '<th>Id</th>'+
        '<th>Created</th>'+
        '<th>Title</th>'+
        '<th>Code</th>'+
        '<th>Linenos</th>'+
        '<th>Language</th>'+
        '<th>Style</th>'+
    '</tr>'

    for (j=0; j<snippets.length; j++) {
         html += '<tr>' +
            '<td>'+ snippets[j]['id']+'</td>'+
            '<td>'+ snippets[j]['created']+'</td>'+
            '<td>'+ snippets[j]['title']+'</td>'+
            '<td>'+ snippets[j]['code']+'</td>'+
            '<td>'+ snippets[j]['linenos']+'</td>'+
            '<td>'+ snippets[j]['language']+'</td>'+
            '<td>'+ snippets[j]['style']+'</td>'+
        '</tr>'
    }
 html += '</table>';
 $('#snippet').append(html);

