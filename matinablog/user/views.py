from django.shortcuts import render, redirect
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from .forms import UserRegisterForm, UserUpdateForm, ProfileUpdateForm
from blog.models import Post
from django.core.mail import send_mail, EmailMessage
from django.conf import settings


def register(request):
    if request.method == 'POST':
        form = UserRegisterForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            messages.success(request, f'Your account is created')
            return redirect('login')
    else:
        form = UserRegisterForm()
    return render(request, 'user/register.html', {'form': form})


@login_required
def profile(request):
    if request.method == 'POST':
        u_form = UserUpdateForm(request.POST, instance=request.user)
        p_form = ProfileUpdateForm(
            request.POST, request.FILES, instance=request.user.profile)
        if u_form.is_valid() and p_form.is_valid():
            u_form.save()
            p_form.save()
            messages.success(request, f'Your account has been updated')
            return redirect('profile')
    else:
        u_form = UserUpdateForm(instance=request.user)
        p_form = ProfileUpdateForm(instance=request.user.profile)

    blog = Post.objects.filter(author=request.user.id)

    context = {
        'u_form': u_form,
        'p_form': p_form,
        'blog': blog,

    }
    return render(request, 'user/profile.html', context)


def email(request):
    subject = 'Thank you for registering to our site'
    message = ' it means a world to us '
    email_from = settings.EMAIL_HOST_USER
    recipient_list = ['dongolmt@gmail.com', ]
    send_mail(subject, message, email_from, recipient_list)
    return redirect('blog-home')
