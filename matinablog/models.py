# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey and OneToOneField has `on_delete` set to the desired behavior
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models


class Post(models.Model):
    post_id = models.AutoField(primary_key=True)
    title = models.CharField(max_length=200)
    contents = models.CharField(max_length=500)
    dateposted = models.DateField(blank=True, null=True)
    author = models.ForeignKey('Userdb', models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'post'


class Userdb(models.Model):
    author_id = models.AutoField(primary_key=True)
    username = models.CharField(max_length=-1)

    class Meta:
        managed = False
        db_table = 'userdb'
